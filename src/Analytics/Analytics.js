import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import './Analytics.css';
import squareimg from './square-logo-light.png';
import spotifyimg from './spotify.png';
import menu from '../menu.png';
import loading from '../Callback/loading.svg';
import axios from 'axios'
import Sidebar from 'react-sidebar';

import blog1 from '../images/blog1.png'
import blog2 from '../images/blog2.png'

import SimpleBarChart from './BarChart';
import Spider from './Spider';
import SimpleAreaChart from './Area';
import { Chart } from 'react-google-charts';

class Dashboard extends Component {

  state ={
    menuOpen:false,
    data: [
            100,120,124,125,321
          ]
  };

  menuToggle() {
    this.setState({menuOpen:!this.state.menuOpen});
  }

  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout();
  }

  componentWillMount() {
    this.setState({ email: '', location_id: '', location_response_state: false, square_connection_status: false, spotify_connection_status: false});
    const { isAuthenticated ,getAccessToken } = this.props.auth;
    if (!isAuthenticated()) {
      this.props.auth.login();
    }
    else {
      const headers = {'Authorization': `Bearer ${getAccessToken()}`};
      console.log(headers);
      axios.get('https://api.exemplar.ai/locations', {headers})
      .then(response => {
        this.setState({location_response_state: true});
        console.log(response);
        if (response.status === 200) {
          this.setState({location_id: response.data[0].id, spotify_block_class: 'integration-block', spotify_button_disable: false, square_connection_status: true})
          if (response.data[0].playHistoryProviders.spotify) {
            this.setState({spotify_connection_status: true});
          }
        }
        else if (response.status === 404) {
          this.setState({ spotify_block_class: 'integration-block-grayout', spotify_button_disable: true});
        }
      })
      .catch(error => {
        this.setState({location_response_state: true});
        console.log(error.response);
        if (error.response.status === 404) {
          this.setState({ spotify_block_class: 'integration-block-grayout', spotify_button_disable: true});
        }
      });
  
      const { userEmail, getEmail } = this.props.auth;
      if (!userEmail) {
        getEmail((err, email) => {
          this.setState({ email: email });
        });
      } else {
        this.setState({ email: userEmail });
      }
    }    
  }

  render() {
     var sidebarContent = <b>Sidebar content</b>;

    const style = {
      position: 'absolute',
      display: 'flex',
      justifyContent: 'center',
      height: '100vh',
      width: '100vw',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'white',
    } 

    const { isAuthenticated } = this.props.auth;
    if (isAuthenticated()) {
      if (this.state.location_response_state) {
        return (
          <div className="container">
           <Sidebar sidebar={sidebarContent}
               open={this.state.menuOpen} docked={this.state.menuOpen}>
              <div className="profile-area">
                <img onClick={() => this.menuToggle()} src={menu} style={{height:20,width:20,position:'relative'}} />
                <div className="settings-form-block">
                  <div className="integration-block">
                    <h4>Recent Tracks</h4>
                      <img src={blog1} style={{height:300,width:250,marginLeft:20}} />
                      <img src={blog2} style={{height:300,width:250,marginLeft:20}} />
                      <img src={blog1} style={{height:300,width:250,marginLeft:20}} />
                      <img src={blog2} style={{height:300,width:250,marginLeft:20}} />
           
                  </div>     
                  <div>
                    <SimpleBarChart color="darkorange"/>
                  </div>
                  <div>
                    <SimpleBarChart color="darkblue"/>
                  </div> 
                  <div>
                    <Spider color="darkorange"/>
                  </div>  
                  <div>
                    <Chart
                      chartType="CandlestickChart"
                      data={[["DAY","val1","val2","val3","val4"],["Mon",20,28,38,45],["Tue",31,38,55,66],["Wed",50,55,77,80],["Thu",77,77,66,50],["Fri",68,66,22,15]]}
                      options={{legend: 'none',
                          bar: { groupWidth: '100%' },
                          candlestick: {
                            fallingColor: { strokeWidth: 0, fill: 'darkorange' }, 
                            risingColor: { strokeWidth: 0, fill: 'darkorange' } 
                          }}}
                      width="50%"
                      height="400px"
                      fill="orange"
                    />
                  </div>
                  <div>
                    <SimpleAreaChart color="darkblue"/>
                  </div>           
                </div>
              </div>
           </Sidebar>
          </div>
        );
      }
      else {
        return (
          <div style={style}>
            <img src={loading} alt="loading"/>
          </div>
        );
      }
    }
    else {
      this.login();
      return (
        <div style={style}>
          <img src={loading} alt="loading"/>
        </div>
      );
    }
  }
}
export default Dashboard;
